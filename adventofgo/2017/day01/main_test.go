package main

import (
	"testing"
)

func Test_partOne(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		wantSum int
	}{
		{"", args{""}, 0},
		{"1122", args{"1122"}, 3},
		{"1111", args{"1111"}, 4},
		{"1234", args{"1234"}, 0},
		{"91212129", args{"91212129"}, 9},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotSum := partOne(tt.args.s); gotSum != tt.wantSum {
				t.Errorf("partOne() = %v, want %v", gotSum, tt.wantSum)
			}
		})
	}
}

func Test_partTwo(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		wantSum int
	}{
		{"", args{""}, 0},
		{"1212", args{"1212"}, 6},
		{"1221", args{"1221"}, 0},
		{"123425", args{"123425"}, 4},
		{"123123", args{"123123"}, 12},
		{"12131415", args{"12131415"}, 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotSum := partTwo(tt.args.s); gotSum != tt.wantSum {
				t.Errorf("partTwo() = %v, want %v", gotSum, tt.wantSum)
			}
		})
	}
}
