package main

import (
	"flag"
	"io/ioutil"
	"log"
)

func main() {
	fn := flag.String("f", "input.txt", "input data file name")
	flag.Parse()

	byt, err := ioutil.ReadFile(*fn)
	if err != nil {
		log.Fatalf("cannot read file %v", *fn)
	}

	s := string(byt)
	log.Printf("part one result: %d", partOne(s))
	log.Printf("part two result: %d", partTwo(s))
}

func partOne(s string) (sum int) {
	return reverseCaptcha(s, 1)
}

func partTwo(s string) (sum int) {
	return reverseCaptcha(s, len(s)/2)
}

func reverseCaptcha(s string, step int) (sum int) {
	if len(s) == 0 {
		return
	}
	for i := 0; i < len(s); i++ {
		pos := (i + step) % len(s)
		if s[i] == s[pos] {
			sum += int(s[i]) - 48
		}
	}
	return
}
