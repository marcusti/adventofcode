package main

import (
	"testing"
)

func Test_partOne(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		wantSum int
	}{
		{"test input", args{`aa bb cc dd ee
aa bb cc dd aa
aa bb cc dd aaa

`}, 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotSum := partOne(tt.args.s); gotSum != tt.wantSum {
				t.Errorf("partOne() = %v, want %v", gotSum, tt.wantSum)
			}
		})
	}
}

func Test_partTwo(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		wantSum int
	}{
		{"test input", args{`abcde fghij
abcde xyz ecdab
a ab abc abd abf abj
iiii oiii ooii oooi oooo
oiii ioii iioi iiio
`}, 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotSum := partTwo(tt.args.s); gotSum != tt.wantSum {
				t.Errorf("partTwo() = %v, want %v", gotSum, tt.wantSum)
			}
		})
	}
}
