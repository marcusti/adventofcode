package main

import (
	"flag"
	"io/ioutil"
	"log"
	"sort"
	"strings"
)

func main() {
	fn := flag.String("f", "input.txt", "input data file name")
	flag.Parse()

	byt, err := ioutil.ReadFile(*fn)
	if err != nil {
		log.Fatalf("cannot read file %v", *fn)
	}

	s := string(byt)
	log.Printf("part I: %d", partOne(s))
	log.Printf("part II: %d", partTwo(s))
}

func partOne(s string) (sum int) {
	return parse(s, func(s string) string { return s })
}

func partTwo(s string) (sum int) {
	return parse(s, func(s string) string { return sortString(s) })
}

func parse(s string, f func(s string) string) (sum int) {
	for _, line := range strings.Split(s, "\n") {
		if len(strings.TrimSpace(line)) == 0 {
			continue
		}
		words := strings.Split(line, " ")
		set := map[string]bool{}
		for _, word := range words {
			set[f(word)] = true
		}
		if len(words) == len(set) {
			sum++
		}
	}
	return
}

func sortString(s string) string {
	ss := strings.Split(s, "")
	sort.Strings(ss)
	return strings.Join(ss, "")
}
