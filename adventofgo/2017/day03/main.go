package main

import (
	"flag"
	"log"
)

func main() {
	i := flag.Int("i", 325489, "input number")
	flag.Parse()

	log.Printf("part one result: %d", partOne(*i))
}

func partOne(num int) int {
	return steps(num)
}

func steps(num int) int {
	if num == 1 {
		return 0
	}
	dim, max := 1, 1
	for max < num {
		dim += 2
		max = dim * dim
	}
	stp := (max - num) % (dim - 1)
	return (dim-1)/2 + abs((dim/2)-stp)
}

func abs(i int) int {
	if i < 0 {
		return i * -1
	}
	return i
}
