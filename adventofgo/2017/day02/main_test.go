package main

import (
	"testing"
)

func Test_partOne(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		wantSum int
	}{
		{"", args{""}, 0},
		{"example", args{`5	1	9	5
7	5	3
2	4	8`}, 18},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotSum := partOne(tt.args.s); gotSum != tt.wantSum {
				t.Errorf("checksum() = %v, want %v", gotSum, tt.wantSum)
			}
		})
	}
}

func Test_partTwo(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		wantSum int
	}{
		{"", args{""}, 0},
		{"example", args{`5	9	2	8
9	4	7	3
3	8	6	5`}, 9},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotSum := partTwo(tt.args.s); gotSum != tt.wantSum {
				t.Errorf("partTwo() = %v, want %v", gotSum, tt.wantSum)
			}
		})
	}
}
