package main

import (
	"flag"
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"strings"
)

func main() {
	fn := flag.String("f", "input.txt", "input data file name")
	flag.Parse()

	byt, err := ioutil.ReadFile(*fn)
	if err != nil {
		log.Fatalf("cannot read file %v", *fn)
	}

	s := string(byt)
	log.Printf("part one result: %d", partOne(s))
	log.Printf("part two result: %d", partTwo(s))
}

func partOne(s string) (sum int) {
	return checksum(s, minMaxDiff)
}

func partTwo(s string) (sum int) {
	return checksum(s, divisor)
}

func checksum(s string, f func(v []int) (r int)) (sum int) {
	if len(s) == 0 {
		return
	}
	for _, line := range strings.Split(s, "\n") {
		v := []int{}
		for _, nums := range strings.Split(line, "\t") {
			num, err := strconv.Atoi(nums)
			if err != nil {
				continue
			}
			v = append(v, num)
		}
		sum += f(v)
	}
	return
}

func minMaxDiff(v []int) (r int) {
	if len(v) == 0 {
		return
	}
	sort.Slice(v, func(i, j int) bool { return v[i] < v[j] })
	return v[len(v)-1] - v[0]
}

func divisor(v []int) (r int) {
	if len(v) == 0 {
		return
	}
	sort.Slice(v, func(i, j int) bool { return v[i] > v[j] })
	for i := 0; i < len(v)/2; i++ {
		for j := i + 1; j < len(v); j++ {
			if v[i]%v[j] == 0 {
				return v[i] / v[j]
			}
		}
	}
	return 0
}
