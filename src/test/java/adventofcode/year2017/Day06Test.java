package adventofcode.year2017;

import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Marcus Titze
 * @since 0.1
 */
public class Day06Test
{
    @Test
    public void cycle()
    {
        assertThat(Day06.cycle(Arrays.asList(0, 2, 7, 0))).isEqualTo(5);
    }
}