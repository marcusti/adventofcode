package adventofcode.year2017;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Marcus Titze
 * @since 0.1
 */
public class Day04Test
{
    @Test
    public void solution1()
    {
        final Day04.Solution solution = Day04.solution("aa bb cc dd ee\naa bb cc dd aa\naa bb cc dd aaa");
        assertThat(solution.partOne).isEqualTo(2);
        assertThat(solution.partTwo).isEqualTo(2);
    }

    @Test
    public void solution2()
    {
        final Day04.Solution solution = Day04.solution("abcde fghij\nabcde xyz ecdab\na ab abc abd abf abj");
        assertThat(solution.partOne).isEqualTo(3);
        assertThat(solution.partTwo).isEqualTo(2);
    }
}