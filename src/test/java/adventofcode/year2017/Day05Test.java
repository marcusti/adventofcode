package adventofcode.year2017;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

/**
 * @author Marcus Titze
 * @since 0.1
 */
public class Day05Test
{
    @Test
    public void partOne()
    {
        assertThat(Day05.partOne(new int[]{0, 3, 0, 1, -3})).isEqualTo(5);
    }

    @Test
    public void partTwo()
    {
        assertThat(Day05.partTwo(new int[]{0, 3, 0, 1, -3})).isEqualTo(10);
    }
}