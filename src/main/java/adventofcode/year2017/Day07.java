package adventofcode.year2017;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day07
{
    public static void main(String[] args)
    {
        final String input = getInput("/2017/day07.txt");
        final Matcher matcher = Pattern.compile("([a-z]+)").matcher(input);
        final Set<String> set = new HashSet<>();
        while (matcher.find())
        {
            final String match = matcher.group();
            if (set.contains(match)) set.remove(match);
            else set.add(match);
        }
        log(set);
    }

    static String getInput(final String fileName)
    {
        final InputStream is = Day06.class.getResourceAsStream(fileName);
        try (final Scanner sc = new Scanner(is).useDelimiter("\\A"))
        {
            return sc.next();
        }
    }

    static void log(final Object o)
    {
        System.out.println(o);
    }
}
