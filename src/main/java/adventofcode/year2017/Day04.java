package adventofcode.year2017;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

class Day04
{
    static class Solution
    {
        public final int partOne;
        public final int partTwo;

        Solution(int partOne, int partTwo)
        {
            this.partOne = partOne;
            this.partTwo = partTwo;
        }
    }

    public static void main(final String[] args)
    {
        final Solution solution = solution(getInput("/2017/day04.txt"));
        System.out.println("part I: " + solution.partOne);
        System.out.println("part II: " + solution.partTwo);
    }

    static Solution solution(final String input)
    {
        int i = 0;
        int ii = 0;
        for (final String line : input.split("\n"))
        {
            final String[] words = line.split("\\s");
            final Set<String> uniques = new HashSet<>(Arrays.asList(words));
            if (words.length == uniques.size()) i++;

            final Set<String> anagrams = Arrays.asList(words).stream()
                    .map(w -> w.chars()
                            .sorted()
                            .collect(
                                    StringBuilder::new,
                                    StringBuilder::appendCodePoint,
                                    StringBuilder::append)
                            .toString())
                    .collect(Collectors.toSet());
            if (words.length == anagrams.size()) ii++;
        }
        return new Solution(i, ii);
    }

    static String getInput(final String fileName)
    {
        final InputStream is = Day06.class.getResourceAsStream(fileName);
        try (final Scanner sc = new Scanner(is).useDelimiter("\\A"))
        {
            return sc.next();
        }
    }
}