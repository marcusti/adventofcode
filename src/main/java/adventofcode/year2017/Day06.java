package adventofcode.year2017;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Day06
{
    public static void main(String[] args)
    {
        final List<Integer> ints = Arrays.stream(getInput("/2017/day06.txt")
                .split("\\s"))
                .mapToInt(Integer::parseInt)
                .boxed()
                .collect(Collectors.toList());

        cycle(Arrays.asList(0, 2, 7, 0));
        cycle(ints);
    }

    static int cycle(final List<Integer> ints)
    {
        log(ints);
        int steps = 0;
        final List<String> seen = new ArrayList<>();
        while (!seen.contains(ints.toString()))
        {
            steps++;
            seen.add(ints.toString());
            final Integer max = Collections.max(ints);
            final int idx = ints.indexOf(max);
            ints.set(idx, 0);
            IntStream.range(idx + 1, idx + 1 + max).forEach(i ->
            {
                final int index = i % ints.size();
                ints.set(index, ints.get(index) + 1);
            });
        }
        log(ints);
        log(String.format("done after %d steps", steps));
        log(String.format("pattern last seen %d steps ago", seen.size() - seen.indexOf(ints.toString())));
        log("");
        return steps;
    }

    static String getInput(final String fileName)
    {
        final InputStream is = Day06.class.getResourceAsStream(fileName);
        try (final Scanner sc = new Scanner(is).useDelimiter("\\A"))
        {
            return sc.next();
        }
    }

    static void log(final Object o)
    {
        System.out.println(o);
    }
}