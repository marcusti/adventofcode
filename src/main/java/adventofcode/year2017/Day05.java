package adventofcode.year2017;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Function;

class Day05
{
    public static void main(String[] args)
    {
        final int[] example = new int[]{0, 3, 0, 1, -3};
        log("part I example:  " + partOne(example.clone()));
        log("part II example: " + partTwo(example));

        final int[] ints = Arrays.stream(getInput("/2017/day05.txt")
                .split("\n"))
                .mapToInt(Integer::parseInt)
                .toArray();

        log("part I solution:  " + partOne(ints.clone()));
        log("part II solution: " + partTwo(ints));
    }

    static int partOne(final int[] ints)
    {
        return jump(ints, i -> i + 1);
    }

    static int partTwo(final int[] ints)
    {
        return jump(ints, i -> i >= 3 ? i - 1 : i + 1);
    }

    static int jump(final int[] ints, final Function<Integer, Integer> f)
    {
        int steps = 0;
        int pos = 0;
        while (pos >= 0 && pos < ints.length)
        {
            int offset = ints[pos];
            ints[pos] = f.apply(offset);
            pos += offset;
            steps++;
        }
        return steps;
    }

    static String getInput(final String fileName)
    {
        final InputStream is = Day06.class.getResourceAsStream(fileName);
        try (final Scanner sc = new Scanner(is).useDelimiter("\\A"))
        {
            return sc.next();
        }
    }

    static void log(final Object o)
    {
        System.out.println(o);
    }
}