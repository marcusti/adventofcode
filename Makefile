DISTDIR := dist

.PHONY: clean
clean:
	rm -rf $(DISTDIR)

.PHONY: test
test: $(DISTDIR)
	cd $(DISTDIR) ; go test ../...

.PHONY: build
build: clean test
	go build -o $(DISTDIR)/day01 adventofgo/2017/day01/main.go
	go build -o $(DISTDIR)/day02 adventofgo/2017/day02/main.go
	go build -o $(DISTDIR)/day03 adventofgo/2017/day03/main.go
	go build -o $(DISTDIR)/day04 adventofgo/2017/day04/main.go

$(DISTDIR):
	mkdir -p $(DISTDIR)
