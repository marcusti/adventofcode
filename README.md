[![pipeline status](https://gitlab.com/marcusti/adventofcode/badges/master/pipeline.svg)](https://gitlab.com/marcusti/adventofcode/commits/master)

Advent of Code solutions in Go and Java

https://adventofcode.com/
